
// Ввод имени

const yourName = document.querySelector(".ade-tiger");
yourName.addEventListener("change", () => {
    if(validate(/^[А-я-іїє]{2,}$/i, yourName.value)) {
        classAdd(yourName);
    } else {
        classRemove(yourName);
    }
})


// Ввод имейла


const yourEmail = document.querySelector(".your-email");
yourEmail.addEventListener("change", () => {
    if(validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, yourEmail.value)) {
        classAdd(yourEmail);
    } else {
        classRemove(yourEmail);
    }
})


// Ввод телефона


const yourPhone = document.querySelector(".your-phone");
yourPhone.addEventListener("change", () => {
    if(validate(/^\+380\d{9}$/, yourPhone.value)) {
        classAdd(yourPhone);
    } else {
        classRemove(yourPhone);
    }
})


//--------------------------------------------


const buttonSignUp = document.querySelector("#btn");
buttonSignUp.addEventListener("click", (e) => {
    if(validate(/^[А-я-іїє]{2,}$/i, yourName.value) !==true || validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, yourEmail.value) !== true || validate(/^\+380\d{9}$/, yourPhone.value) !== true) {
        buttonSignUp.classList.add("error")
        e.preventDefault();
    } else {
        buttonSignUp.classList.remove("error");
        buttonSignUp.classList.add("success");
    }
})


function validate(pattern, value) {
    return pattern.test(value)
};

function classAdd(name) {
    name.classList.remove("error")
    name.classList.add("success")
}

function classRemove(name) {
    name.classList.remove("success")
    name.classList.add("error")
}